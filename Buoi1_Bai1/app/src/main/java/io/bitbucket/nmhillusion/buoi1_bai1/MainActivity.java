package io.bitbucket.nmhillusion.buoi1_bai1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnTest).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "You have clicked the TEST button", Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.btnStar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "You have clicked the IMAGEBUTTON button", Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.btnExit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                System.exit(0);
            }
        });

        CheckBox checkBox = findViewById(R.id.cbAutoSave),
                starCheckBox = findViewById(R.id.cbStar);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    Toast.makeText(MainActivity.this, "ChechBox is checked", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this, "ChechBox is unchecked", Toast.LENGTH_SHORT).show();
                }
            }
        });

        starCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    Toast.makeText(MainActivity.this, "Star style ChechBox is checked", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this, "Star style ChechBox is unchecked", Toast.LENGTH_SHORT).show();
                }
            }
        });

        RadioButton rad1 = findViewById(R.id.opt1);
        final ImageView img1 = findViewById(R.id.img1),
                img2 = findViewById(R.id.img2);
        rad1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    img1.setVisibility(View.VISIBLE);
                    img2.setVisibility(View.GONE);
                }else{
                    img2.setVisibility(View.VISIBLE);
                    img1.setVisibility(View.GONE);
                }
            }
        });

        ToggleButton toggleButton = findViewById(R.id.btnToggle);
        final EditText editText = findViewById(R.id.etxMsg);
        final TextView textView = findViewById(R.id.tvDisplay);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    textView.setText(editText.getText());
                }
            }
        });
    }
}