package io.bitbucket.nmhillusion.buoi1_bai2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.img);

        findViewById(R.id.btnContext).setOnCreateContextMenuListener(this);

        findViewById(R.id.btnExit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                System.exit(0);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean res = super.onCreateOptionsMenu(menu);

        menu.add(0,0,0,"Chon hinh 1");
        menu.add(0,1,1,"Chon hinh 2");
        menu.add(0,2,2,"Chon hinh 3");
        menu.add(0,3,3,"Chon hinh 4");

        return res;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean res = super.onOptionsItemSelected(item);

        switch(item.getItemId()){
            case 0:
                imageView.setImageResource(R.drawable.ca_lia_thia_1);
                break;
            case 1:
                imageView.setImageResource(R.drawable.ca_lia_thia_2);
                break;
            case 2:
                imageView.setImageResource(R.drawable.ca_lia_thia_3);
                break;
            case 3:
                imageView.setImageResource(R.drawable.ca_lia_thia_4);
                break;
        }

        return res;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.add(0,0,0,"Muc thu 1");
        menu.add(0,1,1,"Muc thu 2");
        menu.add(0,2,2,"Muc thu 3");
        menu.add(0,3,3,"Muc thu 4");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        boolean res = super.onContextItemSelected(item);

        switch (item.getItemId()){
            case 0:
                Toast.makeText(this, "Ban chon muc thu 1", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                Toast.makeText(this, "Ban chon muc thu 2", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(this, "Ban chon muc thu 3", Toast.LENGTH_SHORT).show();
                break;
            case 3:
                Toast.makeText(this, "Ban chon muc thu 4", Toast.LENGTH_SHORT).show();
                break;
        }

        return res;
    }
}
